gvm use go1.4
GIT_PATH=$GOPATH/src/gitlab-beta.engr.illinois.edu/mcconne7
PROJECT_PATH=$GIT_PATH/dstore
cd $GIT_PATH
git clone git@gitlab-beta.engr.illinois.edu:mcconne7/dstore.git
cd $PROJECT_PATH
./go_get.sh
go install ./...
