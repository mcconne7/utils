gvm use go
cd $GOPATH/src/gitlab-beta.engr.illinois.edu/mcconne7/dstore
git fetch origin
git rebase origin/master
go get -t -d -u ./...
go install ./...
dstore_server $1 '' 3000 8000 9000 mcconne7@fa15-cs425-g30-01:8000 mcconne7@fa15-cs425-g30-01:9000 data/
