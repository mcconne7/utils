## Installation
This only needs to be run once, but must be run before start_server will
work.

```
. utils/install.sh
```

## Starting a server

```
. utils/start_server.sh PORT_NUMBER
```

For example:

```
. utils/start_server.sh 4567
```
